> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
ドワーフの村よろず屋
> CONTEXT: Map080/display_name/ < UNTRANSLATED
Dwarf Village General Store
> END STRING

> BEGIN STRING
\\C[1]よろず屋
\\C[0]「いらっしゃい！ 景気のいい格好だね～」
> CONTEXT: Map080/events/2/pages/0/5/Dialogue < UNTRANSLATED
> CONTEXT: Map080/events/2/pages/0/14/Dialogue < UNTRANSLATED
\\C[1]Merchant
\\C[0]Welcome! Take your time and buy something.
> END STRING

> BEGIN STRING
\\C[1]商人
\\C[0]「今忙しいから、よそへ行っておくれ！ 」
> CONTEXT: Map080/events/3/pages/0/5/Dialogue < UNTRANSLATED
> CONTEXT: Map080/events/3/pages/0/14/Dialogue < UNTRANSLATED
> CONTEXT: Map063/events/3/pages/0/5/Dialogue < UNTRANSLATED
> CONTEXT: Map063/events/3/pages/0/14/Dialogue < UNTRANSLATED
\\C[1]Merchant
\\C[0]I'm busy now, go elsewhere!
> END STRING

> BEGIN STRING
\\C[1]商人
\\C[0]「ドワーフ達は器用だからな。なんでも特別なアイテムを
　作っているらしいが、私には卸してくれないんだよ！ 」
> CONTEXT: Map080/events/3/pages/0/22/Dialogue < UNTRANSLATED
\\C[1]Merchant
\\C[0]The dwarves are skilled. They make special items 
but they won't sell in bulk! 
> END STRING
