> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
\\C[1]じぃさん
\\C[0]「おぉぉ！ それはっ……！！ まさしく……！！！！ 」
> CONTEXT: Map130/events/2/pages/0/3/Dialogue < UNTRANSLATED
\\C[1]Old man
\\C[0]Ooooh! Thaaat's!! Veeeeery...!!!!
> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あのね、これには色々と事情があるのよ。それより
　お家の人が心配してるわよ？ しばらく帰ってないって…… 」
> CONTEXT: Map130/events/2/pages/0/6/Dialogue < UNTRANSLATED
\\C[1]\\N[1]
\\C[0]You know, a lot of things happened.
Is that person worried about their home?
I haven't been back home in a while either...
> END STRING

> BEGIN STRING
\\C[1]じぃさん
\\C[0]「おぉ！ すっかり忘れておったわい！ 嫁子も
　寂しがっとるじゃろう、帰るとするか」
> CONTEXT: Map130/events/2/pages/0/13/Dialogue < UNTRANSLATED
\\C[1]Old man
\\C[0]Ooooh! I completely forgot! I have a wife back home,
waiting for me. Does she miss me I wonder...
> END STRING

> BEGIN STRING
\\C[1]じぃさん
\\C[0]「ぐへへ……あの嫁も ああ見えて好きモノでなぁ……
　ふぉっふぉっふぉ、たまには可愛がってやらんとな！ 」
> CONTEXT: Map130/events/2/pages/0/17/Dialogue < UNTRANSLATED
\\C[1]Old man
\\C[0]Ehehe... My wife likes that kind of thing too...
　Fuohohoho, it's good to do that once in a while!
> END STRING
