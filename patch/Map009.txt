> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
モリィの部屋
> CONTEXT: Map009/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様……私めの部屋に参られるとは……
　私を食べるおつもりでしょうか？ 」
> CONTEXT: Map009/events/2/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「私は\\N[1]様の従順なる家来に間違いありませんが
　いくら空腹であれ、お兄様が良い顔をしませんぞ」
> CONTEXT: Map009/events/2/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ちっ……違うわよっ！！ ちょっと散歩してただけだってば！！ 」
> CONTEXT: Map009/events/2/pages/0/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様は空腹になると、見境がなくなる傾向が
 ありますから」
> CONTEXT: Map009/events/2/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「人を淫乱ビッチみたいに言わないでよっ！ 」
> CONTEXT: Map009/events/2/pages/0/25/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ここには何もありません！ あまり探し回らないで下さい！！ 」
> CONTEXT: Map009/events/2/pages/2/4/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なんか怪しいわね」
> CONTEXT: Map009/events/2/pages/2/8/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あそこに何か隠してあるわね！ 」
> CONTEXT: Map009/events/2/pages/2/14/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ひぃぃぃぃ～！！ そこは！！ やめて下さい～～っ！！ 」
> CONTEXT: Map009/events/2/pages/2/33/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\N[1]のブロマイドを見つけた……
> CONTEXT: Map009/events/2/pages/2/37/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/37/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「幼き頃の愛くるしい\\N[1]様のお写真ですよっ
　い、い、……いけませんでしたか！？ 」
> CONTEXT: Map009/events/2/pages/2/41/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/41/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……別に……。元に戻しておくわね」
> CONTEXT: Map009/events/2/pages/2/45/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/45/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ふぅ……他のは見つからなくてよかった…… 」
> CONTEXT: Map009/events/2/pages/2/51/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/134/51/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ない！ ない！ どこにもないっ！！ 」
> CONTEXT: Map009/events/2/pages/3/15/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「モリィ、どうしたの？ 」
> CONTEXT: Map009/events/2/pages/3/19/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ないんです！！ 私の大切な \\N[1]様の\\C[2]
　写真\\C[0]コレクションの\\C[2]写真\\C[0]が１枚足りないんです！！ 」
> CONTEXT: Map009/events/2/pages/3/23/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/23/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「コレクション……？ 」
> CONTEXT: Map009/events/2/pages/3/28/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/28/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「はっ！！ ……べ、べべべべべ別に変な\\C[2]写真\\C[0]じゃないんです！
　幼少期からの愛らしいお姿を年代別に並べてですね…… 」
> CONTEXT: Map009/events/2/pages/3/32/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ふ～ん……いいけど。一緒に探そうか？ 」
> CONTEXT: Map009/events/2/pages/3/37/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/37/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「えっ！？ いいのですか！？ 」
> CONTEXT: Map009/events/2/pages/3/41/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/41/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「焦ってるモリィもカワイイけど、私の\\C[2]写真\\C[0]がどっかに行ったまま
　っていうのも、いい気がしないしね 」
> CONTEXT: Map009/events/2/pages/3/45/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/45/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ありがとうございます！ 是非お願いします！
　もし誰かに拾われてしまったらと思うと…… 」
> CONTEXT: Map009/events/2/pages/3/50/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/50/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……どんな\\C[2]写真\\C[0]なのよ」
> CONTEXT: Map009/events/2/pages/3/55/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/55/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト２　モリィの探し物\\C[0]
受注しました。
> CONTEXT: Map009/events/2/pages/3/61/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/130/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「しかし\\N[1]様はいつも、愛くるしゅうございますねぇ 」
> CONTEXT: Map009/events/2/pages/4/4/Dialogue < UNTRANSLATED
> CONTEXT: Map009/events/2/pages/8/60/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「写真じゃなくて、こっち見て言ってよ…… 」
> CONTEXT: Map009/events/2/pages/4/8/Dialogue < UNTRANSLATED
> CONTEXT: Map009/events/2/pages/8/64/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ほらモリィ、写真あったわよ 」
> CONTEXT: Map009/events/2/pages/5/5/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「はぁぁぁ\\I[122] ありがとうございます！ 」
> CONTEXT: Map009/events/2/pages/5/10/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「どれも思い出深い大切な１枚ですからね！
　見つかって良かったです！ ありがとうございました！ 」
> CONTEXT: Map009/events/2/pages/5/13/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「え、えぇ……モリィがいつも写真撮ってたなんて
　知らなかったけど、もう失くさないようにね」
> CONTEXT: Map009/events/2/pages/5/18/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/18/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「はい！ 以後気をつけます！
　ささっ、こちらはお礼の粗品でございます」
> CONTEXT: Map009/events/2/pages/5/23/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/23/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「おら、お礼なんてくれるの？ 気遣わなくていいのに」
> CONTEXT: Map009/events/2/pages/5/29/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「いえいえ、家来の分際で\\N[1]様のお手をわずらわせたゆえ
　少ないですがお納め下さいませ！ 」
> CONTEXT: Map009/events/2/pages/5/32/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そぉ？ じゃ貰っておくね、ありがとう\\I[122] 」
> CONTEXT: Map009/events/2/pages/5/37/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/37/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　　クエスト２　達成！
> CONTEXT: Map009/events/2/pages/5/44/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/131/44/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ちなみに\\N[1]様のお写真を撮る際には、正面
　４５度の他に、後ろ姿など、それから背景は…… 」
> CONTEXT: Map009/events/2/pages/6/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「う、うん……わかったから。いつの間に撮ってたのかしら…… 」
> CONTEXT: Map009/events/2/pages/6/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様……！！ お美しいのは十分承知して
　おりますが……お風邪を引かれては困りますし…… 」
> CONTEXT: Map009/events/2/pages/8/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なに照れてるのよ\\I[122] 」
> CONTEXT: Map009/events/2/pages/8/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「ひぃぃ、\\N[1]様……！！ やはりその格好は
　刺激てきすぎます！ 何を着ても お美しいですけど…… 」
> CONTEXT: Map009/events/2/pages/8/23/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あら、そ～お？\\I[122] 」
> CONTEXT: Map009/events/2/pages/8/28/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
モリィの宝物入れらしい……
> CONTEXT: Map009/events/3/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「モリィの宝物って何だろう？ 」
> CONTEXT: Map009/events/3/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
モリィの宝物入れだ……
> CONTEXT: Map009/events/3/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……そっとしておきましょう 」
> CONTEXT: Map009/events/3/pages/2/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「『忠実なる家臣とは』『素敵な僕（しもべ）』『忠誠心』
　変わった本が多いわね…… 」
> CONTEXT: Map009/events/4/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
モリィのお手入れグッズだ……
> CONTEXT: Map009/events/5/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「肉球クリーム！？ ……か、かわいいじゃない\\I[122] 」
> CONTEXT: Map009/events/5/pages/0/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
モリィのベッドだ……
> CONTEXT: Map009/events/6/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ん？ ベッドの下に何か…… 」
> CONTEXT: Map009/events/6/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「見なかった事にしよう…… 」
> CONTEXT: Map009/events/6/pages/0/11/Dialogue < UNTRANSLATED

> END STRING
