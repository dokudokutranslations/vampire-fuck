> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
モーアビッチの町民家
> CONTEXT: Map066/display_name/ < UNTRANSLATED
Moorvich Townspeople
> END STRING

> BEGIN STRING
\\C[1]女の子
\\C[0]「たまには おばぁちゃんに会いたいなぁ。
　でも森に住んでるから、私だけじゃ行けないし…… 」
> CONTEXT: Map066/events/2/pages/0/3/Dialogue < UNTRANSLATED
\\C[1]Girl
\\C[0]I wish I could see my grandma from time to time.
But I live in the forest, so I can't go alone...
> END STRING

> BEGIN STRING
\\C[1]女の子
\\C[0]「なんで脱いでるの……？ 」
> CONTEXT: Map066/events/2/pages/2/5/Dialogue < UNTRANSLATED
\\C[1]Girl
\\C[0]Why are you undressing...?
> END STRING

> BEGIN STRING
\\C[1]女の子
\\C[0]「アハハ！ へんなの～！ おっぱい見えてるよ！ 」
> CONTEXT: Map066/events/2/pages/2/14/Dialogue < UNTRANSLATED
\\C[1]Girl
\\C[0]Ahaha! It's weird! I can see your breasts!
> END STRING

> BEGIN STRING
\\C[1]女の子
\\C[0]「そういえば、うちって、おじいちゃんは
　どこにいるんだろう……？ 」
> CONTEXT: Map066/events/2/pages/2/22/Dialogue < UNTRANSLATED
\\C[1]Girl
\\C[0]Now that I think about it,
where is grampa...?
> END STRING

> BEGIN STRING
\\C[1]おばさん
\\C[0]「もー、うちの息子ったら、遊んでばっかりなのよ！ 」
> CONTEXT: Map066/events/3/pages/0/3/Dialogue < UNTRANSLATED
\\C[1]Auntie
\\C[0]Well come to my house and play with my son!
> END STRING

> BEGIN STRING
\\C[1]おばさん
\\C[0]「そんな格好でウロチョロしないでおくれ！ 」
> CONTEXT: Map066/events/3/pages/2/5/Dialogue < UNTRANSLATED
> CONTEXT: Map066/events/3/pages/2/14/Dialogue < UNTRANSLATED
\\C[1]Auntie
\\C[0]Well don't just stand there, get going!
> END STRING

> BEGIN STRING
\\C[1]おばさん
\\C[0]「なんだか息子が、マセてきた気がするわぁ。
　何かあったのかしらね？ 」
> CONTEXT: Map066/events/3/pages/2/22/Dialogue < UNTRANSLATED
\\C[1]Auntie
\\C[0]It feels as if my son has somewhat
matured. I wonder, what happened?
> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「さ、さぁ…… 」
> CONTEXT: Map066/events/3/pages/2/28/Dialogue < UNTRANSLATED
\\C[1]\\N[1]
\\C[0]C-come on...
> END STRING
